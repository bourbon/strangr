Use [angr](https://github.com/angr/angr) to symbolically execute a binary and discover any strings that were constructed during (symbolic) runtime.

