from pathlib import Path

import click
import simplejson as json

from strangr import execution, static
from strangr.const import DEFAULT_STRING_LEN


@click.command()
@click.version_option()
@click.argument('sample', type=Path)
@click.option('--json-report', type=Path)
@click.option('--min-length', type=int, default=DEFAULT_STRING_LEN)
def cli(sample: Path, json_report: Path, min_length: int):
    results = {}
    report = {'metadata': {'path': str(sample.resolve()), 'min_length': min_length}, 'results': results}
    sample_buf = sample.read_bytes()

    results['static'] = static.get_all(sample_buf, min_length=min_length)
    results['execution'] = execution.get_all(sample_buf, min_length=min_length)

    print(json.dumps(report, iterable_as_array=True))

    if json_report:
        json.dump(json_report, report, iterable_as_array=True)


if __name__ == "__main__":
    cli()
