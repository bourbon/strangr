import logging
import re
import string

from enum import Enum
from itertools import chain
from typing import AnyStr, Iterator, Match, Pattern

from .const import DEFAULT_STRING_LEN


logger = logging.getLogger(__name__)


class StrType(str, Enum):
    ASCII = '[%s]{%d,}'
    UNICODE = '(?:[%s]\x00){%d,}'


def get_str_pattern(pattern: StrType, *, min_length: int = DEFAULT_STRING_LEN) -> Pattern:
    """
    Compile a regular expression into a `re.Pattern`

    :param pattern: The pattern to use from the ``Pattern`` enum
    :param min_length: Minimum length of string to match
    :return: Compiled `re.Pattern`
    """
    logger.debug('Compiling %s pattern' % pattern.name)
    pat = pattern.value % (string.printable, min_length)
    return re.compile(pat.encode('utf-8'))


def get_strings_iter(buf: bytes, *, str_type: StrType, min_length: int = DEFAULT_STRING_LEN) -> Iterator[Match]:
    """
    Get strings of a specific type as an iterable

    :param buf: The buffer (file bytes) to search
    :param str_type: The type of string to match (``StrType``)
    :param min_length: Minimum length of string to match
    :return: `Iterator` of all found strings
    """
    p = get_str_pattern(str_type, min_length=min_length)
    return p.finditer(buf)


def get_all(buf: bytes, min_length: int = DEFAULT_STRING_LEN) -> Iterator[AnyStr]:
    """
    Get all strings from ``buf`` that are at least ``min_length`` long

    :param buf: The buffer (file bytes) to search
    :param min_length: Minimum length of string to match
    :return: `Iterator` of all found strings
    """
    a_string_iter = get_strings_iter(buf, str_type=StrType.ASCII, min_length=min_length)
    u_string_iter = get_strings_iter(buf, str_type=StrType.ASCII, min_length=min_length)
    for match in chain(a_string_iter, u_string_iter):
        yield match.group(0)
