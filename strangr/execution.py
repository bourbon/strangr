from typing import AnyStr, Iterator

import angr

from .const import DEFAULT_STRING_LEN


def get_all(path: AnyStr, min_length: int = DEFAULT_STRING_LEN) -> Iterator[AnyStr]:
    """
    Symbolically execute ``path`` with `angr` and extract strings that are constructed at run time

    :param path: The sample to symbolically execute
    :param min_length: Minimum length of string to match
    :return: `Iterator` of all found strings
    """
    pass
