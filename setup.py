from setuptools import find_packages, setup


setup(
    name='stangr',
    version='0.1.0',
    description='Find strings in a binary using the angr framework',
    license='LICENSE',
    packages=find_packages(),
    install_requires=[
        'angr',
        'click',
    ],
#    setup_requires=[
#        'pytest-runner',
#    ],
    tests_require=[
        'pytest',
    ],
    entry_points={
        'console_scripts': [
            'strangr = strangr.app:cli',
        ],
    }
)
